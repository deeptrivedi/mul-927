import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/Login";
import ProductDetails from "./components/ProductDetails";
import HomepageHeader from "./components/HomepageHeader";
import Cart from "./components/Cart";
import { Provider, useSelector } from "react-redux";
import store from "./store";
import PlaceOrder from "./components/PlaceOrder";
const App = () => (
  <Provider store={store}>
    <Router>
      <HomepageHeader />
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/home" component={Home} />
        <Route path="/product/:productId" component={ProductDetails} />
        <Route path="/cart" component={Cart} />
        <Route path="/orders" component={PlaceOrder} />
        <Route exact path="/">
          <Redirect to="/login" />
        </Route>
      </Switch>
    </Router>
  </Provider>
);

export default App;
