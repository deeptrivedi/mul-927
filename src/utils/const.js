export const productsData = [
  {
    type: "Living Room",
    data: [
      {
        id: "fabric_sofa",
        title: "Fabric Sofa",
        img: "/LivingRoom/1.jpg",
        rent: {
          month: "4000",
          quaterly: "11000",
          half_yearly: "20000",
          yearly: "38000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "5-7 weeks",
      },
      {
        id: "wooden_sofa",
        title: "Wooden Sofa",
        img: "/LivingRoom/2.jpg",
        rent: {
          month: "6000",
          quaterly: "17000",
          half_yearly: "34000",
          yearly: "60000",
        },
        deposit: "3000",
        seller: "Innovation Arch.",
        rating: "4",
        edt: "1-2 weeks",
      },
      {
        id: "3_seater_sofa",
        title: "3 Seater Sofa",
        img: "/LivingRoom/3.jpg",
        rent: {
          month: "3000",
          quaterly: "11000",
          half_yearly: "17000",
          yearly: "35000",
        },
        deposit: "1000",
        seller: "Strat Security.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "2_Seater_Sofa",
        title: "2 Seater Sofa",
        img: "/LivingRoom/4.jpg",
        rent: {
          month: "2500",
          quaterly: "6000",
          half_yearly: "13000",
          yearly: "25000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "l_shaped_sofa",
        title: "L Shaped Sofa",
        img: "/LivingRoom/5.jpg",
        rent: {
          month: "7000",
          quaterly: "11000",
          half_yearly: "17000",
          yearly: "35000",
        },
        deposit: "1000",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "3_seater_chesterfield_sofa",
        title: "3 Seater Chesterfield Sofa",
        img: "/LivingRoom/6.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Strat Security.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "3_seater_modular_sofa",
        title: "3 Seater Modular Sofa",
        img: "/LivingRoom/7.jpg",
        rent: {
          month: "3000",
          quaterly: "6000",
          half_yearly: "11000",
          yearly: "21000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "3_seater_office_sofa",
        title: "3 Seater Office Sofa",
        img: "/LivingRoom/8.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Innovation Arch.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "3_seater_recliner_sofa",
        title: "3 seater Recliner Sofa",
        img: "/LivingRoom/9.jpg",
        rent: {
          month: "5800",
          quaterly: "15000",
          half_yearly: "20000",
          yearly: "39000",
        },
        deposit: "3000",
        seller: "Eco Focus.",
        rating: "5",
        edt: "2-3 weeks",
      },
      {
        id: "sofa_cum_bed",
        title: "Sofa cum Bed",
        img: "/LivingRoom/10.jpg",
        rent: {
          month: "2500",
          quaterly: "7000",
          half_yearly: "15000",
          yearly: "28000",
        },
        deposit: "2000",
        seller: "Strat Security.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "leather_sofa",
        title: "Leather Sofa",
        img: "/LivingRoom/11.jpg",
        rent: {
          month: "5000",
          quaterly: "13000",
          half_yearly: "28000",
          yearly: "55000",
        },
        deposit: "3000",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "futons",
        title: "Futons",
        img: "/LivingRoom/12.jpg",
        rent: {
          month: "3000",
          quaterly: "11000",
          half_yearly: "20000",
          yearly: "38000",
        },
        deposit: "3000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "ottoman",
        title: "Ottoman",
        img: "/LivingRoom/13.jpg",
        rent: {
          month: "500",
          quaterly: "1300",
          half_yearly: "2500",
          yearly: "5000",
        },
        deposit: "200",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "2_seater_recliner_sofa",
        title: "2 Seater Recliner Sofa",
        img: "/LivingRoom/14.jpg",
        rent: {
          month: "4000",
          quaterly: "12000",
          half_yearly: "23000",
          yearly: "40000",
        },
        deposit: "200",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "2_seater_office_sofa",
        title: "2 Seater Office Sofa",
        img: "/LivingRoom/15.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Strat Security.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "2_seater_chesterfield_sofa",
        title: "2 Seater Chesterfield Sofa",
        img: "/LivingRoom/16.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Eco Focus.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "2_seater_modular_sofa",
        title: "2 Seater Modular Sofa",
        img: "/LivingRoom/17.jpg",
        rent: {
          month: "5000",
          quaterly: "8000",
          half_yearly: "15000",
          yearly: "25000",
        },
        deposit: "1000",
        seller: "Strat Security.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "2_seater_l_shaped_sofa",
        title: "2 Seater L-Shaped Sofa",
        img: "/LivingRoom/18.jpg",
        rent: {
          month: "7000",
          quaterly: "11000",
          half_yearly: "17000",
          yearly: "35000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "2_seater_wooden_sofa",
        title: "2 Seater Wooden Sofa",
        img: "/LivingRoom/19.jpg",
        rent: {
          month: "6000",
          quaterly: "17000",
          half_yearly: "34000",
          yearly: "60000",
        },
        deposit: "3000",
        seller: "Eco Focus.",
        rating: "4",
        edt: "1-2 weeks",
      },
      {
        id: "2_seater_fabric_sofa",
        title: "2 Seater Fabric Sofa",
        img: "/LivingRoom/20.jpg",
        rent: {
          month: "4000",
          quaterly: "11000",
          half_yearly: "20000",
          yearly: "38000",
        },
        deposit: "1000",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "5-7 weeks",
      },
    ],
  },
  {
    type: "Dinning Room",
    data: [
      {
        id: "6_seater_dinning_sets",
        title: "6 Seater Dinning Sets",
        img: "/DinningRoom/1.jpg",
        rent: {
          month: "3000",
          quaterly: "5000",
          half_yearly: "7500",
          yearly: "45000",
        },
        deposit: "1000",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "8_seater_dinning_sets",
        title: "8 Seater Dinning Sets",
        img: "/DinningRoom/2.jpg",
        rent: {
          month: "5000",
          quaterly: "12000",
          half_yearly: "25000",
          yearly: "28000",
        },
        deposit: "2000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "4_seater_dinning_sets",
        title: "4 Seater Dinning Sets",
        img: "/DinningRoom/3.jpg",
        rent: {
          month: "2000",
          quaterly: "5000",
          half_yearly: "10000",
          yearly: "20000",
        },
        deposit: "1000",
        seller: "Strat Security.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "2_seater_dinning_sets",
        title: "2 Seater Dinning Sets",
        img: "/DinningRoom/4.jpg",
        rent: {
          month: "1000",
          quaterly: "2500",
          half_yearly: "7000",
          yearly: "12000",
        },
        deposit: "500",
        seller: "Innovation Arch.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "extendable_dinning_sets",
        title: "Extendable Dinning Sets",
        img: "/DinningRoom/5.jpg",
        rent: {
          month: "7000",
          quaterly: "11000",
          half_yearly: "17000",
          yearly: "35000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "round_dinning_sets",
        title: "Round Dinning Sets",
        img: "/DinningRoom/6.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Innovation Arch.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "dinning_table",
        title: "Dinning Table",
        img: "/DinningRoom/7.jpg",
        rent: {
          month: "3000",
          quaterly: "6000",
          half_yearly: "11000",
          yearly: "21000",
        },
        deposit: "1000",
        seller: "Strat Security.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "dinning_chairs",
        title: "Dinning chairs",
        img: "/DinningRoom/8.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Eco Focus.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "bar_cabinets",
        title: "Bar cabinets",
        img: "/DinningRoom/9.jpg",
        rent: {
          month: "5800",
          quaterly: "15000",
          half_yearly: "20000",
          yearly: "39000",
        },
        deposit: "3000",
        seller: "Innovation Arch.",
        rating: "5",
        edt: "2-3 weeks",
      },
      {
        id: "bar_trolleys",
        title: "Bar Trolleys",
        img: "/DinningRoom/10.jpg",
        rent: {
          month: "2500",
          quaterly: "7000",
          half_yearly: "15000",
          yearly: "28000",
        },
        deposit: "2000",
        seller: "Strat Security.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "cabinets_&_sideboard",
        title: "Cabinets & Sideboard",
        img: "/DinningRoom/11.jpg",
        rent: {
          month: "5000",
          quaterly: "13000",
          half_yearly: "28000",
          yearly: "55000",
        },
        deposit: "3000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "microwave_stands",
        title: "Microwave Stands",
        img: "/DinningRoom/12.jpg",
        rent: {
          month: "3000",
          quaterly: "11000",
          half_yearly: "20000",
          yearly: "38000",
        },
        deposit: "3000",
        seller: "Strat Security.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "kitchen_racks",
        title: "Kitchen Racks",
        img: "/DinningRoom/13.jpg",
        rent: {
          month: "500",
          quaterly: "1300",
          half_yearly: "2500",
          yearly: "5000",
        },
        deposit: "200",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "benches",
        title: "Benches",
        img: "/DinningRoom/14.jpg",
        rent: {
          month: "4000",
          quaterly: "12000",
          half_yearly: "23000",
          yearly: "40000",
        },
        deposit: "200",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "kitchen_trolley",
        title: "Kitchen Trolley",
        img: "/DinningRoom/15.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Innovation Arch.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "bar_stools_and_chair",
        title: "Bar Stools and Chair",
        img: "/DinningRoom/16.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Strat Security.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "2_seater_modular_sofa",
        title: "2 Seater Modular Sofa",
        img: "/DinningRoom/17.jpg",
        rent: {
          month: "5000",
          quaterly: "8000",
          half_yearly: "15000",
          yearly: "25000",
        },
        deposit: "1000",
        seller: "Innovation Arch.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "iconic_chairs",
        title: "Iconic Chairs",
        img: "/DinningRoom/18.jpg",
        rent: {
          month: "7000",
          quaterly: "11000",
          half_yearly: "17000",
          yearly: "35000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "wine_racks",
        title: "Wine Racks",
        img: "/DinningRoom/19.jpg",
        rent: {
          month: "6000",
          quaterly: "17000",
          half_yearly: "34000",
          yearly: "60000",
        },
        deposit: "3000",
        seller: "Strat Security.",
        rating: "4",
        edt: "1-2 weeks",
      },
      {
        id: "kitchen_shelves",
        title: "Kitchen Shelves",
        img: "/DinningRoom/20.jpg",
        rent: {
          month: "4000",
          quaterly: "11000",
          half_yearly: "20000",
          yearly: "38000",
        },
        deposit: "1000",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "5-7 weeks",
      },
    ],
  },
  {
    type: "Bed Room",
    data: [
      {
        id: "queen_size_bed",
        title: "Queen Size Bed",
        img: "/BedRoom/1.jpg",
        rent: {
          month: "3000",
          quaterly: "5000",
          half_yearly: "7500",
          yearly: "45000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "king_size_bed",
        title: "King Size Bed",
        img: "/BedRoom/2.jpg",
        rent: {
          month: "5000",
          quaterly: "12000",
          half_yearly: "25000",
          yearly: "28000",
        },
        deposit: "2000",
        seller: "Strat Security.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "single_beds",
        title: "Single Beds",
        img: "/BedRoom/3.jpg",
        rent: {
          month: "2000",
          quaterly: "5000",
          half_yearly: "10000",
          yearly: "20000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "hydraulic_storage_beds",
        title: "Hydraulic Storage Beds",
        img: "/BedRoom/4.jpg",
        rent: {
          month: "1000",
          quaterly: "2500",
          half_yearly: "7000",
          yearly: "12000",
        },
        deposit: "500",
        seller: "Innovation Arch.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "trundle_beds",
        title: "Trundle Beds",
        img: "/BedRoom/5.jpg",
        rent: {
          month: "7000",
          quaterly: "11000",
          half_yearly: "17000",
          yearly: "35000",
        },
        deposit: "1000",
        seller: "Strat Security.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "double_bed",
        title: "Double Bed",
        img: "/BedRoom/6.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Eco Focus.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "twin_bed",
        title: "Twin Bed",
        img: "/BedRoom/7.jpg",
        rent: {
          month: "3000",
          quaterly: "6000",
          half_yearly: "11000",
          yearly: "21000",
        },
        deposit: "1000",
        seller: "Strat Security.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "divan_bed",
        title: "Divan Bed",
        img: "/BedRoom/8.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Innovation Arch.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "platform_beds",
        title: "Platform Beds",
        img: "/BedRoom/9.jpg",
        rent: {
          month: "5800",
          quaterly: "15000",
          half_yearly: "20000",
          yearly: "39000",
        },
        deposit: "3000",
        seller: "Strat Security.",
        rating: "5",
        edt: "2-3 weeks",
      },
      {
        id: "wardrobes",
        title: "Wardrobes",
        img: "/BedRoom/10.jpg",
        rent: {
          month: "2500",
          quaterly: "7000",
          half_yearly: "15000",
          yearly: "28000",
        },
        deposit: "2000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "table_lamps",
        title: "Table Lamps",
        img: "/BedRoom/11.jpg",
        rent: {
          month: "5000",
          quaterly: "13000",
          half_yearly: "28000",
          yearly: "55000",
        },
        deposit: "3000",
        seller: "Innovation Arch.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "photo_frame",
        title: "Photo Frame",
        img: "/BedRoom/12.jpg",
        rent: {
          month: "3000",
          quaterly: "11000",
          half_yearly: "20000",
          yearly: "38000",
        },
        deposit: "3000",
        seller: "Strat Security.",
        rating: "3",
        edt: "2-3 weeks",
      },
      {
        id: "dressing_table",
        title: "Dressing Table",
        img: "/BedRoom/13.jpg",
        rent: {
          month: "500",
          quaterly: "1300",
          half_yearly: "2500",
          yearly: "5000",
        },
        deposit: "200",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "side_and_end_tables",
        title: "Side and End Tables",
        img: "/BedRoom/14.jpg",
        rent: {
          month: "4000",
          quaterly: "12000",
          half_yearly: "23000",
          yearly: "40000",
        },
        deposit: "200",
        seller: "Strat Security.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "book_shelves",
        title: "Book Shelves",
        img: "/BedRoom/15.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Eco Focus.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "folding_mattress",
        title: "Folding Mattress",
        img: "/BedRoom/16.jpg",
        rent: {
          month: "3800",
          quaterly: "10000",
          half_yearly: "18000",
          yearly: "32000",
        },
        deposit: "2000",
        seller: "Strat Security.",
        rating: "4",
        edt: "2-3 weeks",
      },
      {
        id: "double_mattress",
        title: "Double Mattress",
        img: "/BedRoom/17.jpg",
        rent: {
          month: "5000",
          quaterly: "8000",
          half_yearly: "15000",
          yearly: "25000",
        },
        deposit: "1000",
        seller: "Innovation Arch.",
        rating: "2",
        edt: "1-2 weeks",
      },
      {
        id: "poster_bed",
        title: "Poster Beds",
        img: "/BedRoom/18.jpg",
        rent: {
          month: "7000",
          quaterly: "11000",
          half_yearly: "17000",
          yearly: "35000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "1-2 weeks",
      },
      {
        id: "study_lamps",
        title: "Study lamps",
        img: "/BedRoom/19.jpg",
        rent: {
          month: "6000",
          quaterly: "17000",
          half_yearly: "34000",
          yearly: "60000",
        },
        deposit: "3000",
        seller: "Innovation Arch.",
        rating: "4",
        edt: "1-2 weeks",
      },
      {
        id: "quilts",
        title: "Quilts",
        img: "/BedRoom/20.jpg",
        rent: {
          month: "4000",
          quaterly: "11000",
          half_yearly: "20000",
          yearly: "38000",
        },
        deposit: "1000",
        seller: "Eco Focus.",
        rating: "3",
        edt: "5-7 weeks",
      },
    ],
  },
];
