// import { ActionTypes } from "../actions"
import { productsData } from "../../utils/const";

const initialState = {
  item: productsData,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    //cases...
    case "ALL_DATA":
      return { ...state, item: productsData };
    case "SELECTED_DATA":
      const filterItem = productsData.filter(
        (data) => data.type === action.payload
      );
      return { ...state, item: filterItem };
    case "SEARCH_DATA":
      console.log(action.payload);
      const result = productsData.map((item) => ({
        ...item,
        data: item.data.filter((item) =>
          item.title?.toLowerCase().includes(action.payload?.toLowerCase())
        ),
      }));
      return { ...state, item: result };
    default:
      return { ...state };
  }
};

export default reducer;
