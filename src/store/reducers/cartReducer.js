// import { ActionTypes } from "../actions"

const initialState = {
  cartItem: [],
  orderPlaced: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    //cases...
    case "ADD_CART_ITEM":
      const filterCartItem = state.cartItem.filter((item) => {
        return item.item.title !== action.payload.item.title;
      });
      const updateCartItem = filterCartItem.concat(action.payload);
      return { ...state, cartItem: updateCartItem };
    case "DELETE_CART_ITEM":
      const deleteCartItem = state.cartItem.filter((item) => {
        return item.item.title !== action.payload.item.title;
      });
      return { ...state, cartItem: deleteCartItem };
    case "ORDER_PLACED":
      return { ...state, orderPlaced: true };
    case "BACK_SHOOPING":
      return { ...state, orderPlaced: false };
    case "SUBMIT":
      return { ...state, cartItem: [], orderPlaced: false };
    default:
      return { ...state };
  }
};

export default reducer;
