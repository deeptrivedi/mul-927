import cartReducer from "./cartReducer";
import navReducer from "./navReducer";
import authReducer from "./authReducer";
import { combineReducers } from "redux";

export default combineReducers({
  cart: cartReducer,
  nav: navReducer,
  auth: authReducer,
});
