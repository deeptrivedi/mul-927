import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { Menu, Container, Button } from "semantic-ui-react";

const HomepageHeader = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector((state) => state.auth.isLoggedIn);
  const orderPlaced = useSelector((state) => state.cart.orderPlaced);
  const history = useHistory();
  return loggedIn && !orderPlaced ? (
    <>
      <Menu size="large">
        <Container>
          <Menu.Item
            onClick={() => {
              dispatch({ type: "ALL_DATA" });
              history.push("/home");
            }}
          >
            Home
          </Menu.Item>
          <Menu.Item position="right">
            <Link to="/cart">
              <Button>View Cart</Button>
            </Link>
            <Button
              style={{ marginLeft: "0.5em" }}
              onClick={() => {
                dispatch({ type: "LOGOUT" });
                dispatch({ type: "SUBMIT" });
                history.push("/login");
              }}
            >
              Log out
            </Button>
          </Menu.Item>
        </Container>
      </Menu>
    </>
  ) : (
    <></>
  );
};

export default HomepageHeader;
