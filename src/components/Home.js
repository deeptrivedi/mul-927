import React, { useEffect, useState } from "react";
import { productsData } from "../utils/const";
import { Card, Container, Grid, Header } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import ProductCard from "./ProductCard";
import VerticalMenu from "./VerticalMenu";
import { useSelector } from "react-redux";
const Home = () => {
  const products = useSelector((state) => state.nav.item);

  return (
    <>
      <Grid>
        <Grid.Row>
          <Grid.Column width={3}>
            <VerticalMenu />
          </Grid.Column>
          <Container>
            <Grid.Column width={13}>
              {products &&
                products.map((type) => {
                  return (
                    <div key={type.type}>
                      <Header as="h1" icon textAlign="left">
                        <Header.Content>
                          {type.data.length > 0 ? type.type : ""}
                        </Header.Content>
                      </Header>
                      <hr />
                      <Card.Group itemsPerRow={3}>
                        {type.data.map((item) => {
                          return <ProductCard item={item} />;
                        })}
                      </Card.Group>
                    </div>
                  );
                })}
            </Grid.Column>
          </Container>
        </Grid.Row>
      </Grid>
    </>
  );
};

export default Home;
