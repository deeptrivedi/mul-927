import React, { Component, useState } from "react";
import { Icon, Menu, Form } from "semantic-ui-react";
import { useDispatch } from "react-redux";

const VerticalMenu = () => {
  const [activeItem, setActiveItem] = useState();
  const [query, setQuery] = useState();
  const dispatch = useDispatch();

  // handleItemClick = (e, name) => setActiveItem(name);

  return (
    <Menu vertical>
      <Menu.Item>
        <Form>
          <Form.Input
            placeholder="Search.."
            name="search"
            value={query}
            onChange={(e, value) => {
              setQuery(value.value);
            }}
          />
          <Form.Button
            content="Search"
            onClick={() => dispatch({ type: "SEARCH_DATA", payload: query })}
            primary
            fluid
          />
        </Form>
      </Menu.Item>
      <Menu.Item>
        Products
        <Menu.Menu>
          <Menu.Item
            name="LivingRoom"
            active={activeItem === "LivingRoom"}
            onClick={() => {
              setActiveItem("LivingRoom");
              dispatch({ type: "SELECTED_DATA", payload: "Living Room" });
            }}
          >
            LivingRoom
          </Menu.Item>
          <Menu.Item
            name="BedRoom"
            active={activeItem === "BedRoom"}
            onClick={() => {
              setActiveItem("BedRoom");
              dispatch({ type: "SELECTED_DATA", payload: "Bed Room" });
            }}
          >
            BedRoom
          </Menu.Item>
          <Menu.Item
            name="DiningRoom"
            active={activeItem === "DiningRoom"}
            onClick={() => {
              setActiveItem("DiningRoom");
              dispatch({ type: "SELECTED_DATA", payload: "Dinning Room" });
            }}
          >
            DinningRoom
          </Menu.Item>
        </Menu.Menu>
      </Menu.Item>

      <Menu.Item
        name="browse"
        active={activeItem === "browse"}
        onClick={() => {
          setActiveItem("browse");
          dispatch({ type: "ALL_DATA" });
        }}
      >
        <Icon name="grid layout" />
        Browse
      </Menu.Item>
    </Menu>
  );
};

export default VerticalMenu;
