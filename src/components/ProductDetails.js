import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation, useParams } from "react-router-dom";
import {
  Grid,
  Image,
  Header,
  Item,
  Form,
  Radio,
  Label,
  Button,
  Icon,
  Rating,
} from "semantic-ui-react";

const ProductDetails = () => {
  const location = useLocation();
  const params = useParams();
  const [data, setData] = useState();
  const [value, setValue] = useState("month");
  const [count, setCount] = useState(1);

  const dispatch = useDispatch();

  const addToCart = () => {
    return (
      <Button
        primary
        floated="right"
        onClick={() => {
          dispatch({
            type: "ADD_CART_ITEM",
            payload: { item: { ...data, quantity: count, rent_type: value } },
          });
        }}
      >
        Add to Cart
        <Icon name="right chevron" />
      </Button>
    );
  };

  const quantityCount = () => {
    return (
      <Button.Group floated="right">
        <Button
          onClick={() => {
            setCount(count - 1);
          }}
        >
          -
        </Button>
        <Button.Or text={count} />
        <Button
          onClick={() => {
            setCount(count + 1);
          }}
        >
          +
        </Button>
      </Button.Group>
    );
  };

  useEffect(() => {
    if (location.state && location.state.data) {
      setData(location.state.data);
    } else {
      console.log(location);
    }
    if (location.state.data.quantity) {
      setCount(location.state.data.quantity);
    }
  }, [location]);
  useEffect(() => {
    if (count <= 0) {
      setCount(0);
    }
  }, [count]);
  return data ? (
    <>
      <Grid celled>
        <Grid.Row>
          <Grid.Column width={5}>
            <Image src={data.img} size="medium" centered />
          </Grid.Column>
          <Grid.Column width={10}>
            <Item>
              <Item.Content>
                <Item.Header as="h1">{data.title}</Item.Header>
                <Item.Meta>
                  <span className="cinema">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua.
                  </span>
                </Item.Meta>
                <Item.Description>
                  <Form.Field
                    control={Radio}
                    value="month"
                    label={`Rs ${data.rent.month} / month`}
                    checked={value === "month"}
                    onChange={() => {
                      setValue("month");
                    }}
                  />
                  <Form.Field
                    control={Radio}
                    value="quaterly"
                    label={`Rs ${data.rent.quaterly} / quaterly`}
                    checked={value === "quaterly"}
                    onChange={() => {
                      setValue("quaterly");
                    }}
                  />
                  <Form.Field
                    control={Radio}
                    value="half_yearly"
                    checked={value === "half_yearly"}
                    label={`Rs ${data.rent.half_yearly} / half year`}
                    onChange={() => {
                      setValue("half_yearly");
                    }}
                  />
                  <Form.Field
                    control={Radio}
                    value="yearly"
                    label={`Rs ${data.rent.yearly} / year`}
                    checked={value === "yearly"}
                    onChange={() => {
                      setValue("yearly");
                    }}
                  />
                </Item.Description>
                <Item.Description>Deposit: Rs {data.deposit}</Item.Description>
                <Item.Description>Seller Name :{data.seller}</Item.Description>
                <Rating
                  icon="star"
                  defaultRating={data.rating}
                  maxRating={5}
                  disabled
                />
                <Item.Extra>
                  {addToCart()} {quantityCount()}
                  <Label>Estimated Date of Delivery</Label>
                  <Label content={data.edt} />
                </Item.Extra>
              </Item.Content>
            </Item>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </>
  ) : (
    <h1>Wait</h1>
  );
};

export default ProductDetails;
