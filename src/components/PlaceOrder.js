import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Button, Header } from "semantic-ui-react";
import { Link, useLocation, useHistory } from "react-router-dom";

const PlaceOrder = () => {
  const cartItem = useSelector((state) => state.cart.cartItem);
  const [subTotal, setSubTotal] = useState(0);
  const [gst, setGst] = useState(0);
  const location = useLocation();
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (location.state) {
      setGst(location.state.gst);
      setSubTotal(location.state.subTotal);
    }
  }, [location]);
  return (
    <div>
      <Header as="h1" icon textAlign="center">
        <Header.Content>Final Order</Header.Content>
      </Header>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Item Name</Table.HeaderCell>
            <Table.HeaderCell>Rent</Table.HeaderCell>
            <Table.HeaderCell>Quantity</Table.HeaderCell>
            <Table.HeaderCell>Price</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {cartItem.map((item) => {
            return (
              <Table.Row key={item.item.id}>
                <Table.Cell>
                  <Link
                    to={{
                      pathname: `/product/${item.item.id}`,
                      state: { data: item.item },
                    }}
                  >
                    {item.item.title}
                  </Link>
                </Table.Cell>
                <Table.Cell>
                  Rs {item.item.rent[item.item.rent_type]} /{" "}
                  {item.item.rent_type}
                </Table.Cell>
                <Table.Cell>{item.item.quantity}</Table.Cell>
                <Table.Cell>
                  Rs {item.item.rent[item.item.rent_type] * item.item.quantity}
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
        <Table.Footer fullWidth>
          <Table.Row>
            <Table.HeaderCell>Sub-Total</Table.HeaderCell>
            <Table.HeaderCell colSpan="2"></Table.HeaderCell>
            <Table.HeaderCell>Rs. {subTotal}</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            <Table.HeaderCell>GST</Table.HeaderCell>
            <Table.HeaderCell colSpan="2"></Table.HeaderCell>
            <Table.HeaderCell>Rs. {gst}</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            <Table.HeaderCell>Total</Table.HeaderCell>
            <Table.HeaderCell colSpan="2"></Table.HeaderCell>
            <Table.HeaderCell>Rs. {subTotal + gst}</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>

      <Link to="/home">
        <Button
          floated="right"
          primary
          onClick={() => {
            dispatch({ type: "SUBMIT" });
          }}
        >
          Submit
        </Button>
      </Link>
      <Link to="/home">
        <Button
          floated="left"
          primary
          onClick={() => {
            dispatch({ type: "BACK_SHOOPING" });
          }}
        >
          Back to Shooping
        </Button>
      </Link>
    </div>
  );
};

export default PlaceOrder;
