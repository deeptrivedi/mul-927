import React from "react";
import { Card, Image, Rating, Button, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

const ProductCard = (props) => {
  return (
    <Card key={props.key}>
      <Image
        style={{ height: "200px", objectFit: "contain", overflow: "hidden" }}
        src={props.item.img}
        wrapped
        ui={false}
      />
      <Card.Content>
        <Card.Header>{props.item.title}</Card.Header>
        <Card.Meta>
          <Rating
            icon="star"
            defaultRating={props.item.rating}
            maxRating={5}
            disabled
          />
        </Card.Meta>
        <Card.Description>
          RENT Yearly: {props.item.rent.yearly}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        Estimated time : {props.item.edt}
        <Link
          to={{
            pathname: `/product/${props.item.id}`,
            state: { data: props.item },
          }}
        >
          <Button floated="right">
            <Icon name="shop" />
          </Button>
        </Link>
      </Card.Content>
      {/* <Link
        to={{
          pathname: `/product/${props.item.id}`,
          state: { data: props.item },
        }}
      >
        <Button animated="vertical">
          <Button.Content hidden>Add to Cart</Button.Content>
          <Button.Content visible>
            <Icon name="shop" />
          </Button.Content>
        </Button>
      </Link> */}
    </Card>
  );
};

export default ProductCard;
