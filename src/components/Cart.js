import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Button } from "semantic-ui-react";
import { Link, useHistory } from "react-router-dom";

const Cart = () => {
  const cartItem = useSelector((state) => state.cart.cartItem);
  const [subTotal, setSubTotal] = useState(0);
  const [gst, setGst] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    let temp = 0;
    cartItem.map((item) => {
      temp = temp + item.item.rent[item.item.rent_type] * item.item.quantity;
    });
    setSubTotal(temp);
    setGst((temp * 12) / 100);
  }, [cartItem]);
  return (
    <div>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Item Name</Table.HeaderCell>
            <Table.HeaderCell>Rent</Table.HeaderCell>
            <Table.HeaderCell>Quantity</Table.HeaderCell>
            <Table.HeaderCell>Price</Table.HeaderCell>
            <Table.HeaderCell>Delete Item</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {cartItem.map((item) => {
            return (
              <Table.Row key={item.item.id}>
                <Table.Cell>
                  <Link
                    to={{
                      pathname: `/product/${item.item.id}`,
                      state: { data: item.item },
                    }}
                  >
                    {item.item.title}
                  </Link>
                </Table.Cell>
                <Table.Cell>
                  Rs {item.item.rent[item.item.rent_type]} /{" "}
                  {item.item.rent_type}
                </Table.Cell>
                <Table.Cell>{item.item.quantity}</Table.Cell>
                <Table.Cell>
                  Rs {item.item.rent[item.item.rent_type] * item.item.quantity}
                </Table.Cell>
                <Table.Cell>
                  <Button
                    onClick={() => {
                      dispatch({ type: "DELETE_CART_ITEM", payload: item });
                    }}
                  >
                    Delete
                  </Button>
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
        <Table.Footer fullWidth>
          <Table.Row>
            <Table.HeaderCell>Sub-Total</Table.HeaderCell>
            <Table.HeaderCell colSpan="2"></Table.HeaderCell>
            <Table.HeaderCell>Rs. {subTotal}</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <Link
        to={{
          pathname: "/orders",
          state: { gst: gst, subTotal: subTotal },
        }}
      >
        <Button
          floated="right"
          primary
          onClick={() => {
            dispatch({ type: "ORDER_PLACED", payload: true });
          }}
        >
          Place Orders
        </Button>
      </Link>
    </div>
  );
};

export default Cart;
